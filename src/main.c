#include <getdns/getdns.h>
#include <getdns/getdns_ext_libevent.h>

#include <event2/event.h>

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

void resolve_cdnskey_record(const char* domain, int timeout_ms, int break_loop_if_solved);

int main()
{
    fprintf(stderr, "Do not break the loop when the request is resolved.\n");
    resolve_cdnskey_record("oknarakovnik.cz", 100, 0);
    fprintf(stderr, "\nBreak the loop as soon as the request is resolved.\n");
    resolve_cdnskey_record("oknarakovnik.cz", 100, 1);
    return EXIT_SUCCESS;
}

typedef struct Solver
{
    const char* domain;
    int timeout_ms;
    struct event_base* event_base_ptr;
    getdns_context* context_ptr;
    getdns_transaction_t task_id;
    int solved;
} Solver;

void getdns_callback_function(
        getdns_context* context_ptr,
        getdns_callback_type_t callback_type,
        getdns_dict* answer,
        void* user_data,
        getdns_transaction_t transaction_id);

int loop(const Solver* solver);
void must_be_good(getdns_return_t result, const char* file, int line);
#define MUST_BE_GOOD(RESULT) must_be_good((RESULT), __FILE__, __LINE__)

void resolve_cdnskey_record(const char* domain, int timeout_ms, int break_loop_if_solved)
{
    Solver solver;
    solver.event_base_ptr = event_base_new();
    solver.domain = domain;
    solver.timeout_ms = timeout_ms;
    MUST_BE_GOOD(getdns_context_create(&solver.context_ptr, 1));
    solver.solved = 0;
    MUST_BE_GOOD(getdns_context_set_timeout(solver.context_ptr, solver.timeout_ms));
    getdns_transport_list_t udp_first[2] = { GETDNS_TRANSPORT_UDP, GETDNS_TRANSPORT_TCP };
    MUST_BE_GOOD(getdns_context_set_dns_transport_list(solver.context_ptr, 2, udp_first));
    getdns_dict* const extensions = getdns_dict_create();
    MUST_BE_GOOD(getdns_dict_set_int(extensions, "dnssec_return_only_secure", GETDNS_EXTENSION_TRUE));
    MUST_BE_GOOD(getdns_extension_set_libevent_base(solver.context_ptr, solver.event_base_ptr));
    solver.task_id = 0;
    MUST_BE_GOOD(getdns_general(solver.context_ptr, solver.domain, GETDNS_RRTYPE_CDNSKEY, extensions, (void*)&solver, &solver.task_id, getdns_callback_function));
    getdns_dict_destroy(extensions);
    int in_progress_loops = 0;
    int solved_loops = 0;
    while (loop(&solver) == 0)
    {
        if (solver.solved)
        {
            if (break_loop_if_solved)
            {
                fprintf(stderr, "The getdns_context_destroy function will (almost certainly) crash.\n");
                break;
            }
            ++solved_loops;
        }
        else
        {
            ++in_progress_loops;
        }
    }
    fprintf(stderr, "in_progress %d loops\nsolved %d loops\n", in_progress_loops, solved_loops);
    getdns_context_destroy(solver.context_ptr);
    event_base_free(solver.event_base_ptr);
}

void getdns_callback_function(
        getdns_context* context_ptr,
        getdns_callback_type_t callback_type,
        getdns_dict* answer,
        void* user_data,
        getdns_transaction_t transaction_id)
{
    Solver* const solver = (Solver*)user_data;
    if (solver->task_id != transaction_id)
    {
        fprintf(stderr, "no task found\n");
        return;
    }
    if (solver->context_ptr != context_ptr)
    {
        fprintf(stderr, "different context\n");
    }
    solver->solved = 1;
    switch (callback_type)
    {
        case GETDNS_CALLBACK_COMPLETE:
        {
            uint32_t status = 0;
            MUST_BE_GOOD(getdns_dict_get_int(answer, "status", &status));
            getdns_dict_destroy(answer);
            fprintf(stdout, "\"%s\" resolved: %u\n", solver->domain, status);
            return;
        }
        case GETDNS_CALLBACK_CANCEL:
            fprintf(stdout, "\"%s\" cancelled\n", solver->domain);
            return;
        case GETDNS_CALLBACK_TIMEOUT:
            fprintf(stdout, "\"%s\" timed out\n", solver->domain);
            return;
        case GETDNS_CALLBACK_ERROR:
            fprintf(stdout, "\"%s\" failed\n", solver->domain);
            return;
    }
    fprintf(stdout, "\"%s\" unexpected error\n", solver->domain);
}

void exit_failure(const char* msg, const char* file, int line) __attribute__ ((__noreturn__));

int loop(const Solver* solver)
{
    switch (event_base_loop(solver->event_base_ptr, EVLOOP_ONCE))
    {
        case 0:
            return 0;
        case 1:
            return 1;
        case -1:
            exit_failure("loop failure", __FILE__, __LINE__);
    }
    exit_failure("loop failure: unexpected return code", __FILE__, __LINE__);
}

void must_be_good(getdns_return_t result, const char* file, int line)
{
    switch(result)
    {
        case GETDNS_RETURN_GOOD:
            return;
        case GETDNS_RETURN_GENERIC_ERROR:
            exit_failure(GETDNS_RETURN_GENERIC_ERROR_TEXT, file, line);
        case GETDNS_RETURN_BAD_DOMAIN_NAME:
            exit_failure(GETDNS_RETURN_BAD_DOMAIN_NAME_TEXT, file, line);
        case GETDNS_RETURN_BAD_CONTEXT:
            exit_failure(GETDNS_RETURN_BAD_CONTEXT_TEXT, file, line);
        case GETDNS_RETURN_CONTEXT_UPDATE_FAIL:
            exit_failure(GETDNS_RETURN_CONTEXT_UPDATE_FAIL_TEXT, file, line);
        case GETDNS_RETURN_UNKNOWN_TRANSACTION:
            exit_failure(GETDNS_RETURN_UNKNOWN_TRANSACTION_TEXT, file, line);
        case GETDNS_RETURN_NO_SUCH_LIST_ITEM:
            exit_failure(GETDNS_RETURN_NO_SUCH_LIST_ITEM_TEXT, file, line);
        case GETDNS_RETURN_NO_SUCH_DICT_NAME:
            exit_failure(GETDNS_RETURN_NO_SUCH_DICT_NAME_TEXT, file, line);
        case GETDNS_RETURN_WRONG_TYPE_REQUESTED:
            exit_failure(GETDNS_RETURN_WRONG_TYPE_REQUESTED_TEXT, file, line);
        case GETDNS_RETURN_NO_SUCH_EXTENSION:
            exit_failure(GETDNS_RETURN_NO_SUCH_EXTENSION_TEXT, file, line);
        case GETDNS_RETURN_EXTENSION_MISFORMAT:
            exit_failure(GETDNS_RETURN_EXTENSION_MISFORMAT_TEXT, file, line);
        case GETDNS_RETURN_DNSSEC_WITH_STUB_DISALLOWED:
            exit_failure(GETDNS_RETURN_DNSSEC_WITH_STUB_DISALLOWED_TEXT, file, line);
        case GETDNS_RETURN_MEMORY_ERROR:
            exit_failure(GETDNS_RETURN_MEMORY_ERROR_TEXT, file, line);
        case GETDNS_RETURN_INVALID_PARAMETER:
            exit_failure(GETDNS_RETURN_INVALID_PARAMETER_TEXT, file, line);
        case GETDNS_RETURN_NOT_IMPLEMENTED:
            exit_failure(GETDNS_RETURN_NOT_IMPLEMENTED_TEXT, file, line);
    }
    exit_failure("Unexpected getdns_return_t value", file, line);
}

void exit_failure(const char* msg, const char* file, int line)
{
    fprintf(stderr, "At %s:%d \"%s\"\n", file, line, msg);
    _exit(EXIT_FAILURE);
}
